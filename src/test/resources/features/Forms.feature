Feature: Forms Screen

  Scenario: User clicks forms button on navigation bar to visit the forms screen

    When I launch wdio app

    And I click "Forms" button

    Then "Forms-screen" page is displayed