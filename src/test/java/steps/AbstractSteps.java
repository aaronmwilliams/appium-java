package steps;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class AbstractSteps {

    AndroidDriver driver = null;
    DesiredCapabilities capabilities = new DesiredCapabilities();

    private static String browserStackUserName;
    private static String browserStackPassword;
    private static String testEnvironment;

    public AbstractSteps() {
        readProperties();
        launchApp();
    }

    private void readProperties() {
        testEnvironment = System.getProperty("test.environment");
        browserStackUserName = System.getProperty("browserstack.username");
        browserStackPassword = System.getProperty("browserstack.password");
    }

    protected AndroidDriver launchApp() {
        if (testEnvironment.equals("local")) {
            return setupLocally();
        } else {
            return setupBrowserStack();
        }
    }

    private AndroidDriver setupBrowserStack() {
        capabilities.setCapability("device", "Google Nexus 6");
        capabilities.setCapability("os_version", "6.0");
        capabilities.setCapability("app", "bs://dc");
        try {
            driver = new AndroidDriver(
                    new URL("https://" + browserStackUserName
                            + ":" + browserStackPassword
                            + "@hub-cloud.browserstack.com/wd/hub"), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.manage().timeouts().implicitlyWait(160, TimeUnit.SECONDS);
        return driver;
    }

    private AndroidDriver setupLocally() {
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", "6.0");
        capabilities.setCapability("deviceName", "Google Nexus 6");
        capabilities.setCapability("app",
                System.getProperty("project.dir") + "/app/app.apk");
        try {
            driver = new AndroidDriver(
                    new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        return driver;
    }
}