package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.assertTrue;

public class NavigationBarSteps extends AbstractSteps {

    @When("^I launch wdio app$")
    public void iLaunchWdioApp() {

    }

    @And("^I click \"(.*)\" button$")
    public void iClickFormsButton(String buttonID) {
        driver.findElementByAccessibilityId(buttonID).click();
    }

    @Then("^\"(.*)\" page is displayed$")
    public void formsScreenPageIsDisplayed(String pageWrapper) {
        assertTrue(driver.findElementByAccessibilityId(pageWrapper).isDisplayed());
        driver.quit();
    }

}
