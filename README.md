# appium-java

### Purpose of project
Demonstrate a simple test framework using Appium with BrowserStack App Automate

### Installing and Running
Import into your IDE as a Gradle project.

#### Running on local emulator

You will need to have an Android emulator running with the following specifications (use Appium for this):

```
{
  "platformName": "Android",
  "platformVersion": "6.0",
  "deviceName": "Nexus 6 API 23",
  "automationName": "App",
  "app": "[PATH TO YOUR PROJECT/appium-java/app/app.apk]"
}
``` 

Run the tests using the following command: `./gradlew test --info -Dtest.environment=local`

#### Running on BrowserStack App Automate

Make sure the .apk located in `/app` is uploaded to your BrowserStack. In `AbstractSteps.java` replace the app capability to the App ID you received after uploading.

Run the tests by the following command: `./gradlew test --info -Dtest.environment=browserstack -Dbrowserstack.username=[YOUR BROWSERSTACK USERNAME] -Dbrowserstack.password=[YOUR BROWSERSTACK PASSWORD]`
